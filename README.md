# Space Rave Theme

A dark and techy theme with a blue background and bright, neon-like colors.

## Available for...
* Text editors
  * Sublime Text
* Terminals
  * Alacritty
  * iTerm2
  * Konsole
  * Mintty
  * PuTTY
  * Terminal.app
  * Xresources

More platforms to be added

## License
MIT License - Copyright (c) 2015 - 2019 Younes Zakaria (drcd) <yz@lp0.dk>

See the [LICENSE](LICENSE) file for details.
